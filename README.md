# Project Title

"Reduxstagram" Redux and React App 

# Description

A simple photo app that will simplify the core ideas behind Redux, React Router and React.js.

Video tutorials to learn how to build JavaScript apps with React.js and Redux by [Wes Bos](https://learnredux.com/)

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces
* [React Router](https://github.com/ReactTraining/react-router) - Declarative routing for React
* [Redux](https://redux.js.org/) - Predictable state container for JavaScript apps


## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details