import React, {PropTypes, Component} from 'react'
import Comment from './Comment'

class Comments extends Component{
    handleSubmit = (e) => {
        e.preventDefault();
        const { postId } = this.props.params
        const author = this.refs.author.value
        const comment = this.refs.comment.value
        this.props.addComment(postId, author, comment)
        this.refs.commentForm.reset()
    }

    render() {
        return (
            <div className="comment">
                {this.props.postComments.map((comment, index) => {
                  return <Comment {...this.props} index={index} key={index} comment={comment} />
                })}
                <form className="comment-form" ref="commentForm" onSubmit={this.handleSubmit}>
                    <input type="text" ref="author" placeholder="Author"/>
                    <input type="text" ref="comment" placeholder="Comment"/>
                    <input type="submit" hidden/>
                </form>
            </div>
        )
    }
}

Comments.propTypes = {
    postComments: PropTypes.array.isRequired,
}

export default Comments
