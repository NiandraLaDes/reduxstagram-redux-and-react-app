import React from 'react'
import Photo from './Photo'
import Comments from './Comments'

const Single = props => {
    const postId = props.params.postId
    const index = props.posts.findIndex((post) => post.code === postId);
    const post = props.posts[index]
    const postComments = props.comments[postId] || []
    return (
        <div className="single-photo">
            <Photo {...props} post={post} index={index}/>
            <Comments {...props} postComments={postComments} />
        </div>
    )
}

export default Single
