import React, {PropTypes} from 'react'

const Comment = props => {
    return (
        <div className="comment">
            <p>
                <strong>{props.comment.user}</strong>
                {props.comment.text}
                <button className="remove-comment" onClick={props.removeComment.bind(null, props.params.postId, props.index)}>&times;</button>
            </p>
        </div>
    )
}

Comment.propTypes = {
    comment: PropTypes.object.isRequired
}

export default Comment
