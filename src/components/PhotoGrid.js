import React from 'react'
import Photo from './Photo'

const PhotoGrid = props => {
    return (
        <div className="photo-grid">
        {
            props.posts.map((post, index) => {
                return <Photo {...props} key={post.code} index={index} post={post}/>
            })
        }
        </div>
    )
}

PhotoGrid.propTypes = {}

export default PhotoGrid
