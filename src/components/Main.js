/* eslint-disable */

import React, { Component } from 'react';
import { Link } from 'react-router'
import css from '../styles/style.styl'

class Main extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
            <Link to="/"><h1>Reduxstagram</h1></Link>
        </div>
        <div className="app-main">
            {React.cloneElement(this.props.children, this.props)}
        </div>
      </div>
    );
  }
}

export default Main;
