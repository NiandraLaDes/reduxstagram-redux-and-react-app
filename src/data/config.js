import Raven from 'raven-js';

const sentry_key = 'a032169ab4164ee8aab7726bd7e186b7';
const sentry_app = '151878';
export const sentry_url = `https://${sentry_key}@sentry.io/${sentry_app}`;

export function logException(ex, context) {
  Raven.captureException(ex, {
    extra: context
  });
  /*eslint no-console:0*/
  window && window.console && console.error && console.error(ex);
}

Raven.setUserContext({
    email: 'bartvanenter@hotmail.com',
})