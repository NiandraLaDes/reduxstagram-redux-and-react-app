const postComments = (state = [], action) => {
    switch(action.type) {
        case "ADD_COMMENT":
            return [
                ...state,
                {
                    "text": action.comment,
                    "user": action.author,
                }
            ]
        case "REMOVE_COMMENT":
            console.log('remove comment: ' + action.index);
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index + 1)
            ]
        default:
            return state
    }
}

const comments = (state = [], action) => {
    if(typeof action.postId !== 'undefined') {
        return {
            ...state,
            [action.postId]: postComments(state[action.postId], action)
        }
    }
    return state
}

export default comments