// import React from 'react';
import ReactDOM from 'react-dom';
import routes from './routes'

import Raven from 'raven-js'
import { sentry_url } from './data/config'
Raven.config(sentry_url).install()

ReactDOM.render(
    routes,
    document.getElementById('root')
);